/**
 * Created by Edmund on 10/8/2015.
 */
angular
    .module("portfolio",
        [
            "ripples",
            "jquery-inject",
            "ngMaterial",
            "ngRoute",
            "examples"
        ]);

angular.module("portfolio").config(["$routeProvider", "$mdThemingProvider", function($routeProvider, $mdThemingProvider){
    "use strict";

    $routeProvider.
        when("/", {
            templateUrl: "sections/home/home-view.html"
        }).
        when("/examples", {
            templateUrl: "sections/examples/examples-view.html",
            controller: "examplesController",
            controllerAs: "examplesCtrl"
        }).
        when("/resume", {
            templateUrl: "sections/resume/resume-view.html"
        }).
        otherwise({
            redirectTo: "/"
        });

    $mdThemingProvider
        .theme("default")
        .primaryPalette('deep-purple')
        .accentPalette('lime');



}]);