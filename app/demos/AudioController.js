
var AudioController = function () {
	this._settings = {};

	this.Init = function (beatResolution) {
		window.AudioContext = window.AudioContext || window.webkitAudioContext;
		var $this = this;
		$this._settings.Context = new AudioContext();
		$this._settings.TrackList = [];
		$this._settings.BeatResolution = beatResolution;

		$this.CatchAnimationFrame = function () {
			window.requestAnimationFrame($this.CatchAnimationFrame);
			for (var i = 0; i < $this._settings.TrackList.length; i++) {
				var thisTrack = $this._settings.TrackList[i];
				thisTrack._analyzeFrame(10, .05, $this._settings.BeatResolution);
			}

			if ($this._settings.EveryFrame) {
				$this._settings.EveryFrame();
			}
		};

		$this.CatchAnimationFrame();
	}
	
	this.StopAll = function(callback){
		
		for (var i = 0; i < this._settings.TrackList.length; i++) {
			var thisTrack = this._settings.TrackList[i];
			thisTrack.Stop();
		}
		
	}

	this.SetOnEveryFrame = function (callback) {
		this._settings.EveryFrame = callback;
	};

	this.PlayTrackList = function (tracks) {
		this._loadTrackList(tracks, this._connectBufferedTracksToDestination)
	};

	this._loadTrackList = function (tracks, callback) {
		var $this = this;
		var allLoadedTracks = [];
		for (var i = 0; i < tracks.length; i++) {
			var thisTrack = tracks[i];
			this._loadSingleTrack(thisTrack, function (loadedTrack) {
				allLoadedTracks.push(loadedTrack);
				if (allLoadedTracks.length == tracks.length) {
					callback($this, allLoadedTracks);
				}
			});
		}
	};

	this._loadSingleTrack = function (track, callback) {
		var $this = this;

		if (track.TrackUrl)
		{
			var request = new XMLHttpRequest();
			request.open('GET', track.TrackUrl, true);
			request.responseType = 'arraybuffer';

			request.onload = function () {
				$this._settings.Context.decodeAudioData(request.response, function (buffer) {
					var bufferSource = $this._settings.Context.createBufferSource()
					bufferSource.buffer = buffer;
					track.SetBufferSource(bufferSource);
					$this._settings.TrackList.push(track);
					callback(track);
				}, function () { alert("Error loading " + track.TrackUrl) });
			};

			request.send();
		}
		else if (track.BufferData) {
			$this._settings.Context.decodeAudioData(track.BufferData, function (buffer) {
				var bufferSource = $this._settings.Context.createBufferSource()
				bufferSource.buffer = buffer;
				track.SetBufferSource(bufferSource);
				$this._settings.TrackList.push(track);
				callback(track);
			}, function () { alert("Error loading track from file buffer") });
		}
	};

	this._connectBufferedTracksToDestination = function (currentController, bufferedTracks, callback) {

		var $this = currentController;

		for (var i = 0; i < bufferedTracks.length; i++) {
			var thisBufferedTrack = bufferedTracks[i];
			if (thisBufferedTrack._settings.Source) {
				var newGainControl = $this._settings.Context.createGain();
				var newAnalyzer = $this._settings.Context.createAnalyser();

				thisBufferedTrack._settings.Source.connect(newGainControl);
				newGainControl.connect(newAnalyzer);

				thisBufferedTrack.SetGainNode(newGainControl);
				thisBufferedTrack.SetAnalyzer(newAnalyzer);

				newAnalyzer.connect($this._settings.Context.destination);

			}
		}

		$this._callStartOnAllTracks();
	};

	this._callStartOnAllTracks = function () {
		for (var i = 0; i < this._settings.TrackList.length; i++) {
			var thisTrack = this._settings.TrackList[i];
			thisTrack.Play(false);
		}
	};


};
