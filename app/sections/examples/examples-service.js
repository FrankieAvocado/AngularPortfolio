/**
 * Created by Edmund on 10/8/2015.
 */
angular
    .module("examples")
    .factory("examplesService", ["$q", function($q) {
        // if I had time, these would go into a database and be accessed via ngResource calls.
        // I'm a little pressed for time though, so uhhh...they aren't  :-D
        // the least I can do is return promises, though.

        var personalExamples = {
            name: "Personal Samples",
            description: "These are personal projects that I was the sole developer on.",
            examples:
            [
				{
					name: "BrewCastle",
					image: "content/images/brewcastle.jpg",
					description: "BrewCastle is a game in which you brew delicious beer to combat the forces of evil and the perils of being thirsty!  It is written in Unity3D, hand-drawn by myself using PyxelEdit and will be releasing on the Steam platform for Windows PC.",
					link: "http://www.brewcastlegame.com" 
				},
                {
                    name: "Horse Puncher",
                    image: "content/images/horse_puncher.png",
                    description:  "Horse Puncher is a 2D side-scrolling game written in Unity3D (C#).  It is a comedic adventure about a land filled with rude horses and one brave man willing to punish them for their transgressions.  It is currently available for download from the Windows 10 app store.",
                    link: "https://www.microsoft.com/en-us/store/games/horse-puncher/9nblggh5z8zb"
                },
                {
                    name: "Heart Beats",
                    image: "content/images/heart_beats.png",
                    description: "Heart Beats is an audio visualizer that plays mp3s and provides dynamic visual feedback.  It is written in JavaScript for the HTML5 Canvas and Audio Context.  You will need a supported browser to play it, I recommend Chrome.",
                    link: "./demos/heartbeats.html"
                },
                {
                    name: "Soggy Bottom Brewery",
                    image: "content/images/soggy_bottom.png",
                    description: "Soggy Bottom Brewery is a local brewery that several friends started in early 2014.  I offered to build their website in exchange for free beer.  Currently they are between hosting providers so I have configured them on a cloud service called AppHarbor.  This site is written in MVC / C# and uses Handlebars templates with AJAX to load in social feeds.",
                    link: "http://www.soggybottombrewery.com"
                },
                {
                    name: "Arbitrary Rating",
                    image: "content/images/arbitrary_rating.png",
                    description: "Arbitrary Rating is just a Ruby on Rails experiment that allows a user to produce rating images in a similar to how placehold.it produces placeholder images.  There are currently only two icon images available:  hamsters and cookies.  No, I don't know why I chose those.  This is just an experiment so the images aren't commercially available.",
                    link: "http://www.arbitraryrating.com/"
                },
                {
                    name: "Angular Tetris Clone",
                    image: "content/images/tetris_clone.png",
                    description: "This very basic (and quite buggy) clone of Tetris is something I wrote in order to test Angular as a vessel for HTML5 canvas projects.  It was a very fun adventure and you can download the source for yourself from my GitHub account.",
                    link: "http://tetrisclone.herokuapp.com"
                }
            ]
        };

        var professionalExamples = {
            name: "Professional Samples",
            description: "These are examples of work that I performed as part of a team for a company.",
            examples:
            [

                {
                    name: "Benchmark Corporate",
                    image: "content/images/benchmark_corporate.png",
                    description: "While I was working at FKQ Marketing, I was the lead developer for Benchmark International's corporate website.  I was responsible for architecting the database structure, the back-end services and controllers and also most of the interactive JavaScript on the site.",
                    link: "http://www.benchmarkcorporate.com/"
                },
                {
                    name: "Raymond James Investor Access",
                    image: "content/images/investor_access.png",
                    description: "During my time at Raymond James, I worked as an Application Developer on the Investor Access team.  We rewrote the app from its original origins as an ASP.Net WebForms app, converting vast swaths of it over to the new MVC system.  I was responsible for working in all layers of the app.",
                    link: "https://investoraccess.rjf.com/"
                },
                {
                    name: "AutoLoop: AutoBook",
                    image: "content/images/auto_book.png",
                    description: "As part of my work at Loop LLC, I worked heavily on the AutoBook dealership software.  This software provides scheduling and management functionality to car-dealerships.  While I was on the AutoBook team, I was responsible for producing code in all layers of the product.",
                    link: "http://www.autoloop.com/solutions/book.aspx"
                }
            ]
        };

        var allExamples = [personalExamples, professionalExamples];

        var query = function(){

            return $q(function(resolve, reject){
                if(allExamples.length > 0){
                    resolve(allExamples);
                }
                else
                {
                    reject({message: "Couldn't find any examples in the database!"});
                }
            });


        };

        var find = function(id){
            return $q(function(resolve, reject){
                var returnMe = null;
                // this is quite ugly and can be solved by implementing some fun
                // underscore stuff, but I don't have a ton of time at time moment :-/
                for (var i = 0, j = allExamples.length; i < j; i++){
                    for(var x = 0, z = allExamples[i].length; x < z; x++){
                        if(allExamples[i][x].id === id){
                            returnMe = allExamples[i][x];
                        }
                    }
                }

                if(returnMe){
                    resolve(returnMe);
                }
                else{
                    reject({message: "Couldn't find that example in the database!"});
                }
            });
        };

        return {
          query: query,
          find: find
        };

    }]);
