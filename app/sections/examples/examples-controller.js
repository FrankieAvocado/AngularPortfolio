/**
 * Created by Edmund on 10/8/2015.
 */
angular
    .module("examples")
    .controller("examplesController", ["examplesService", function(examplesService){
        var vm = this;
        vm.exampleGroups = [];

        examplesService
            .query()
            .then(function(successResult){
                    vm.exampleGroups = successResult;
                }, function(errorResult){
                    console.log("Oh dear, an error occurred: " + errorResult.message);
                }
            );
    }]);