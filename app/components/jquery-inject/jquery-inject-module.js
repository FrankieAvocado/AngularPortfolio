/**
 * Created by Edmund on 10/8/2015.
 */
// apparently I foolishly used jquery when I first made my ripple effect directive.
// I shall minimize the evil that this causes by using dependency injection
// on a jquery module.

angular.module("jquery-inject", []);