/**
 * Created by Edmund on 10/8/2015.
 * Copied from my Cascade of Leaves site.
 */
angular
    .module("ripples")
    .directive("hasRipples", ["$", function ripplesDirective($){
        "use strict";
        return{
            restrict: "A",
            link: function(scope, element){
                var inkElement = angular.element("<span class='ink'></span>");
                var target = $(element[0]);
                element.prepend(inkElement);
                target.on('click', function(e){
                    var targetInk = target.find('.ink');
                    targetInk.removeClass("animate");
                    if(!targetInk.height() && !targetInk.width()){
                        var size = Math.max(target.outerWidth(), target.outerHeight());
                        targetInk.css({height: size, width: size});
                    }

                    var x = e.pageX - target.offset().left - targetInk.width()/2;
                    var y = e.pageY - target.offset().top - targetInk.height()/2;

                    targetInk.css({top: y + 'px', left: x + 'px'}).addClass('animate');
                });
            }
        };
}]);