var express = require("express");
var app = express();

app.use(express.static(__dirname + "/app"));
console.log("activating express...");
app.listen(process.env.PORT || 3000);